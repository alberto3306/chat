import json
import uuid
from collections import defaultdict

from channels import Group
from channels.handler import AsgiHandler
from django.http.response import HttpResponse

def get_room(name):
    return Group('room.%s' % name)


def send_message(room, text):
    
    message = {
        'uuid': uuid.uuid4().hex,
        'text': text,
    }

    get_room(room).send({
        'text': json.dumps(message),
    })


def ws_connect(message, chatroom):
    message.reply_channel.send({'accept': True})
    get_room(chatroom).add(message.reply_channel)
    send_message(chatroom, 'Client %s joined the chatroom.' % message.reply_channel)


def ws_receive(message, chatroom):
    send_message(chatroom, message.content['text'])

def ws_disconnect(message, chatroom):
    get_room(chatroom).discard(message.reply_channel)
    send_message(chatroom, 'Client %s leaved the chatroom.' % message.reply_channel)
