from channels.routing import route

from .consumers import ws_connect, ws_disconnect, ws_receive

channel_routing = [
    route('websocket.connect', ws_connect, path=r'^/(?P<chatroom>\w+)/$'),
    route('websocket.receive', ws_receive, path=r'^/(?P<chatroom>\w+)/$'),
    route('websocket.disconnect', ws_disconnect),
]
