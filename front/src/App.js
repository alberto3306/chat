import React, { Component } from 'react'
import './App.css'
import OnlineStatus from './OnlineStatus'

class App extends Component {

    state = {
        socket: null, // Websocket to send and receive messages
        messages: [], // Message list to be rendered
    }

    /* COMPONENT LIFECYCLE */

    render() {
        const messages = this.state.messages.map((message) => {
            return <p key={message.uuid}>{message.text}</p>
        })

        return (
            <div className="App">
                <OnlineStatus connected={this.isConnected()} />
                {messages}
                <form onSubmit={(e) => this.formSubmitHandler(e)}>
                    <input id="enter-text-box" type="text" size="100" autoFocus="autofocus" autoComplete="off" />
                    <button type="submit">Send</button>
                </form>
            </div>
        )
    }

    componentDidMount() {
        this.connect()
    }

    /* METHODS */

    formSubmitHandler(e) {
        e.preventDefault()
        const inputBox = document.getElementById('enter-text-box')
        this.sendMessage(inputBox.value)
        inputBox.value = ''
        return false
    }

    sendMessage(text) {
        this.state.socket.send(text)
    }

    connect() {
        if (!this.isConnected()) {
            this.setState({
                socket: this.createSocket(),
            })
        }
    }

    isConnected() {
        return (this.state.socket != null && this.state.socket.readyState === WebSocket.OPEN)
    }

    createSocket() {
        // FIXME: hardcoded socket URL
        const socket = new WebSocket('ws://' + window.location.hostname + ':8000/hello/')
        socket.onmessage = (e) => this.handleSocketMessage(e)
        socket.onopen = (e) => this.handleSocketOpen(e)
        socket.onclose = (e) => this.handleSocketClose(e)
        return socket
    }

    /* EVENT HANDLERS */

    eventMessage(message) {

        // TODO: validate this!
        const messageObject = JSON.parse(message)

        this.setState({
            messages: [
                ...this.state.messages.slice(-9),
                messageObject,
            ]
        })
    }

    eventConnected() {
        console.log('connected')
    }

    eventDisconnected() {
        console.log('disconnected')
    }

    /* SOCKET HANDLERS */

    handleSocketMessage(e) {
        console.log('Socket message received.')
        console.log(e)
        this.eventMessage(e.data)
    }

    handleSocketOpen(e) {
        console.log('Socket opened.')
        console.log(e.target)
        this.eventConnected()
    }

    handleSocketClose(e) {
        console.log('Socket closed.')
        console.log(e.target)
        this.eventDisconnected()
    }

}

export default App;
