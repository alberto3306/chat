import React from 'react'

const onlineStatus = (props) => {

    const labelClass = props.connected ? 'label label-success' : 'label label-danger';
    const labelLabel = props.connected ? 'Online' : 'Offline';

    return (
        <div className="online-status">
            <span className={labelClass}>{labelLabel}</span>
        </div>
    )
}

export default onlineStatus